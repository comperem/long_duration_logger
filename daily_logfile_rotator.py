#!/usr/bin/env python3
#
# daily_logfile_rotator.py is a class that allows another long-duration python
# program to log to filenames that rotate every day (or minute or month) with the new date as prefix.
#
# the longDurationLogger() class also updates two symbolic links:
#   - to the current logfile : current_weather_data_logfile,csv
# and
#   - to the previous logfile: previous_weather_data_logfile.csv
#
# these two symlinks facilitate plot creation of the last 24 hours of weather
# data that spans 2 files
#
# an example use program is in the __main__ below.
#
# tips:
# - test with strftimeStr set to change every minute, then change to every day when launching long-duration process
# - decide if you need fdLog.flush() or not (prefer not unless your cases has slow data logging and you want tail -f to update)
#
#
# ---
# Copyright 2023-2024 Marc Compere
#
# daily_logfile_rotator_v3.py is free software which means
# you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# ---
#
# Marc Compere, comperem@erau.edu
# created : 07 Sep 2023
# modified: 05 Jan 2024


import os # to check for and create /var/tmp/wx; also os.symlink()
import sys
import time
import logging
import threading
import queue
import socket # for myGetHostname()
import csv # for DictWriter logging to .csv
from datetime import datetime

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] [%(threadName)-10s] %(message)s')

class longDurationLogger:
    
    def __init__(self,logDir,fieldnames,strftimeStr,doFlush=False):
        logging.debug('longDurationLogger init')
        self.logDir        = logDir
        self.fieldnames    = fieldnames
        self.nameCheckCnt  = 0 # name check counter (counts # of checks to seeif name should change)
        self.nameChangeCnt = 0 # name change counter
        self.errCnt        = 0 # aggregate indicator of all warnings or errors
        self.strftimeStr   = strftimeStr
        self.doFlush       = doFlush #True # flush file every write loop? yes to see immediately with tail -f; no for sd card longevity
        
        # make sure base logfile dir is there and writeable
        self.checkOrMakeLogDir(logDir)
        
        self.ipAddrStr=self.myGetHostname() # not needed but helps debugging to know your ip being used
        
        # open new logfile and report if it already exists (for restart condition)
        self.open_new_logfile_at_current_time()
        fNameStart = self.fNamePrefix # to start logfile_name_updater() thread
        
        self.e = threading.Event() # exit event for all threads
        self.qFname = queue.Queue() # que visitble by all methods and logfile_name_updater() thread
        debugLogRot =1 #0 # (0/1) logfile name rotator thread debug flag
        self.debug = 1 #0 (0/1) logfile writer debug flag
        logging.debug('longDurationLogger() constructor: self.doFlush={0}, debugLogRot={1}, self.debug={2}'.format(self.doFlush, debugLogRot, self.debug))
        
        # start filename checker and rotator thread
        t2 = threading.Thread(name='logfile_name_updater', target=self.logfile_name_updater, args=(fNameStart,self.qFname,self.ipAddrStr,debugLogRot))
        t2.start()
    
    # -----------------------------------------------------------
    # method: make sure logDir is there; if not create it
    def checkOrMakeLogDir(self,logDir):
        if os.path.isdir(logDir)==False:
            logging.debug('[{0}] not found! creating it...'.format(logDir))
            try:
                os.mkdir(logDir)
                logging.debug('success.')
            except:
                logging.debug('barf - could not create [{0}]'.format(logDir))
                exit(-1)
        return 0
    # -----------------------------------------------------------
    
    
    # method: get and return IP address
    def myGetHostname(self):
        # from: https://www.geeksforgeeks.org/display-hostname-ip-address-python/
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # connect() for UDP doesn't send packets
        s.connect(('10.0.0.0', 0)) 
        return s.getsockname()[0]
    
    
    # method: open a new long-duration cellular actor logfile in logDir (e.g. /var/tmp/wx)
    def open_new_logfile_at_current_time(self):
        tNowFname   = datetime.now()
        self.fNamePrefix = tNowFname.strftime(self.strftimeStr)
        
        # os.path.join() intelligently adds '/' or not, even on windows; https://stackoverflow.com/a/15010678/7621907
        csvNameLog  = os.path.join(self.logDir , '{0}_weather_station_data.csv'.format(self.fNamePrefix) ) # logfile name changes each run: /tmp/wx/2023_08_07__14_46_45_weather_station_data.csv
        
        self.fileExists=os.path.isfile(csvNameLog) # does this file already exist before open()? perhaps from restarting the logger with an existing log; append but do not add filedname header
        
        try:
            self.fdLog = open(csvNameLog,'a') # this will open a new file or an existing file to resume from a previously stopped log
        except:
            logging.debug("Unexpected error:", sys.exc_info()[0])
            logging.debug("error occurred when trying to create a new logfile - check disk space - update try-except with this exception just printed above")
            exit(-1)
        self.firstLogLine    = True # write csv header before logging first message
        logging.debug('\t{0}: logging cellular actor positon data to: {1}'.format(sys.argv[0],csvNameLog))
        
        # Create a symbolic link from src pointing to dst using os.symlink()
        # symbolic link is for web display to report logfile size (in csv_file_to_bokeh_dataTable_d.py) 
        dstPrev = os.path.join(self.logDir , 'previous_weather_data_logfile.csv' ) # <- this never changes: /var/tmp/wx/previous_weather_data_logfile.csv
        dstCur  = os.path.join(self.logDir , 'current_weather_data_logfile.csv' )  # <- this never changes: /var/tmp/wx/current_weather_data_logfile.csv
        
        try:
            if os.path.exists(dstPrev):
                os.remove(dstPrev)
                logging.debug('\tremoved previous symbolic link: {0}'.format(dstPrev))
        except:
            self.errCnt+=1
            logging.debug('warning: errCnt={0}, could not delete previous logfile symbolic link (a), {1} --> {2}'.format(self.errCnt, csvNameLog,dstPrev))
            logging.debug("         sys.exec_info:", sys.exc_info()[0])
        
        try:
            if os.path.exists(dstCur):
                os.rename(dstCur,dstPrev) # rename current to previous log symlink
                logging.debug('\trenamed current to previous sym link: {0} --> {1}'.format(dstCur,dstPrev))
        except:
            self.errCnt+=1
            logging.debug('warning: errCnt={0}, could not rename prev to current logfile symlink (b), {1} --> {2}'.format(self.errCnt,csvNameLog,dstCur))
            logging.debug("         sys.exec_info:", sys.exc_info()[0])
        
        try:
            os.symlink(csvNameLog, dstCur) # ln -s src dst
            logging.debug('\tnew current sym link created successfully: {0} --> {1}'.format(csvNameLog,dstCur))
        except:
            logging.debug('warning: errCnt={0}, symbolic link failed! (c), {1} --> {2}'.format(self.errCnt,csvNameLog,dstCur))
            logging.debug('\n\tis another cellular data listener running? --> ~/stop_all.sh')
            logging.debug("         sys.exec_info:", sys.exc_info()[0])
    
    
    
    # ------------------------------------------------------------------------------
    # *thread* logfile_name_updater() that enters a while loop to stay alive
    #  and periodically check if the long-duration logfile name should change; alive until exit event
    def logfile_name_updater(self,fNameStart,qFname,ipAddrStr,debug):
        if debug>0: logging.debug('Starting logfile name checker and setter thread')
        
        fNamePrefix = fNameStart # initialize a long-duration thread that changes filenames every time strftimeStr is different (every minute or day, however strftimeStr is defined)
        while self.e.is_set() is False:
            tNow      = datetime.now() # local tNow
            #                strftime(''%Y_%m_%d__%H_%M_%S')
            fNameTest = tNow.strftime(self.strftimeStr) # strftime() = string format time: str='2018_07_24__18_18_40'
            if debug>0: logging.debug(" ")
            if debug>0: logging.debug("fNamePrefix   = [{0}]".format(fNamePrefix))
            if debug>0: logging.debug("fNameTest     = [{0}]".format(fNameTest))
            
            self.nameCheckCnt+=1
            if (fNamePrefix != fNameTest):
                if debug>0: logging.debug("\n\n\n\n\n\n")
                if debug>0: logging.debug("    bingo! date changed!")
                fNamePrefix=fNameTest
                self.nameChangeCnt+=1
                if debug>0: logging.debug( "[{0}], nameCheckCnt={1}, nameChangeCnt={2}, ip={3} sending new logfile prefix to data collector thread: [{4}]".format(tNow,self.nameCheckCnt,self.nameChangeCnt,ipAddrStr,fNamePrefix) )
                qFname.put(fNamePrefix)
                if debug>0: logging.debug("\n\n\n\n\n\n")
            else:
                if debug>0: logging.debug( "checked, but no logfile name change, self.nameCheckCnt={0}, self.nameChangeCnt={1}".format(self.nameCheckCnt, self.nameChangeCnt))
            
            #time.sleep(1)
            time.sleep(10)
        logging.debug('thread exiting')


    
    # method: high frequency logfile writer and name rotator; designed to be ticked every loop
    def writeLogfile(self,myDataDict):
        tNow = myDataDict['tNow'] #tNow='{0}'.format(datetime.now())    # '2023-09-07 09:36:36.741496'
        if self.debug>1: logging.debug('[{0}] writing to file [{2}]]'.format(tNow,myFile.name)) # high frequency output; at rate of data logging
        
        # check to see if filename prefix changed (at midnight every day to rotate the logfile)
        while self.qFname.empty()==False:
            fNamePrefixUnused = self.qFname.get() # name inside message is unused; message arrival triggers new logfile rotation
            self.nameChangeCnt+=1
            if self.debug>0: logging.debug("\n\n\n\tnameChangeCnt={0}, got qFname message with new filename: [{1}]".format(self.nameChangeCnt,fNamePrefixUnused) )
            if self.debug>0: logging.debug("\tclosing current filename: [{0}]".format(self.fdLog.name))
            self.fdLog.close()
            if self.debug>0: logging.debug("\tpreparing new log file")
            self.open_new_logfile_at_current_time()
            if self.debug>0: logging.debug("\tdone. new logfile ready for writing: [{0}]".format(self.fdLog.name))
            if self.debug>0: logging.debug("\n\n\n")
        
        if (self.firstLogLine==True):
            #fieldnames = [ 'vid', 'vehName', 'vehType', 'runState', 'bIdCmd', 'bIdName', 'srt_err_avg', 'tStamp', 'gps_unixTime', 'pos', 'vel', 'u', 't', 'detectMsg', 'routeStr' ] # specify dictionary field order; veh time unimportant; tStamp is core's common recording time for all vehicles
            # create fieldnames from the dictionary with lists and dictionaries
            if self.debug>0: logging.debug('logging fieldnames               : {0}'.format(self.fieldnames))
            #self.dwLog = csv.DictWriter( self.fdLog, fieldnames=self.fieldnames, delimiter='|', quoting=csv.QUOTE_NONE, escapechar=' ', extrasaction='ignore' ) # inhibits quotes in .csv; ignores fields not in fieldnames
            #self.dwLog = csv.DictWriter( self.fdLog, fieldnames=self.fieldnames, delimiter='|', extrasaction='ignore', dialect='unix') # dialect='unix' prevents windows cr-lf and ^M characters; ignores fields not in fieldnames
            #self.dwLog = csv.DictWriter( self.fdLog, fieldnames=self.fieldnames, delimiter=',', extrasaction='ignore', dialect='unix') # dialect='unix' prevents windows cr-lf and ^M characters; ignores fields not in fieldnames
            #self.dwLog = csv.DictWriter( self.fdLog, fieldnames=self.fieldnames, delimiter=',', dialect='unix') # dialect='unix' prevents windows cr-lf and ^M characters; ignores fields not in fieldnames
            self.dwLog = csv.DictWriter( self.fdLog, fieldnames=self.fieldnames, delimiter=',') # dialect='unix' prevents windows cr-lf and ^M characters; ignores fields not in fieldnames
            if self.debug>0: logging.debug('logging data to   : {0}'.format(self.fdLog.name))
            if not(self.fileExists):
                self.dwLog.writeheader() # write header if this is a new file; if this file exists, do not insert a header partially down the .csv that already contains data
            else:
                if self.debug>0: logging.debug("logfile existed before open(), so did *not* write dictwriter field names")
                if self.debug>0: logging.debug("--> assuming this is a restarted logger script appending to an existing logfile")
            
            if self.debug>0: logging.debug('data logging fieldnames: {0}'.format(self.fieldnames))
            self.firstLogLine=False # only write header once
        
        # write data to .csv logfile
        try:
            self.dwLog.writerow( myDataDict )
            if self.doFlush==True:
                self.fdLog.flush() # flush() forces the write at each line - without flush the OS buffers and writes occasionally
        except ValueError as err:
            print('\n\n')
            print('\t\tbarf - if file opened properly, then you probably tried to write field names')
            print('\t\t       that were inconsistent with your initialization, which was:')
            print('\n\t\t\tfieldnames={0}'.format( self.fieldnames ))
            print('\n\t\tfieldnames provided in myDataDict:')
            for k, v in myDataDict.items():
                print("\t\t\t{0}".format(k))
            print('\t\tmake these match!\nexiting!')
            self.e.set() # exit threads
            exit(-2)
        except:
            logging.debug("Unexpected error:", sys.exc_info()[0])
            self.e.set() # exit threads
            exit(-1)







# --------------------------------------------------------------------
# -------------   main loop is not needed for library    -------------
# -------------   but provides a simple usage example    -------------
# --------------------------------------------------------------------
if __name__ == "__main__":
    
    import sys
    import time
    import random
    import logging
    import threading
    from datetime import datetime
    from daily_logfile_rotator import longDurationLogger as ldl
    
    logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] [%(threadName)-10s] %(message)s')
    
    loopCnt=0
    tStart=time.time()
    debug=1 #0 # main()'s debug flag
    dt=2 # (s)
    skipNth=1000 # print every N'th counter value
    e = threading.Event()

    # ----- longDurationLogger setup -----
    # define comparison string; this determines log rotation every minute, hour, or day
    strftimeStr='%Y_%m_%d__%H_%M' # str='2018_07_24__18_18'  <-- test with this b/c it changes every minute
    #strftimeStr='%Y_%m_%d'        # str='2018_07_24'         <-- run for daily logfile rotation because it changes every day

    logDir='/var/tmp/wx'
    fieldnames=['tNow','Temp_C','Pressure_Pa','RH']
    doFlush=False #True # flush file every write loop? yes to see immediately with tail -f; no for sd card longevity
    myLdl = ldl(logDir,fieldnames,strftimeStr,doFlush) # ldl-->long duration logger with filename update each day and symbolic link to current file
    
    try:
        logging.debug('Starting data collector loop')
        
        if debug>0: logging.debug("{0} listening...   loopCnt={1}, ipAddrStr={2}".format(sys.argv[0],loopCnt,myLdl.ipAddrStr))
        while e.is_set() is False:
            # print counter in a way that reduces terminal scrolling
            if (loopCnt % skipNth)==0:
                logging.debug('[{0}] long running process count {1}'.format(myLdl.ipAddrStr,loopCnt))
            else:
                print('{0},'.format(loopCnt),flush=True,end='')
            loopCnt+=1
            
            # collect data here; fictitious examples with random()
            T      = random.uniform(0.0,50.0)        # (deg C)
            P      = random.uniform(1013.05,1013.50) # (hPa) and (mBar) standard pressure at sea-level is 1013.25 in both millibars (mb) and hectopascal (hPa)
            RH     = random.uniform(+40,+100)        # (%)
            tNow   = '{0}'.format(datetime.now())    # '2023-09-07 09:36:36.741496'
            myDataDict={'tNow':datetime.now(), 'Temp_C':T, 'Pressure_Pa':P,'RH':RH}
            
            if debug>1:
                for k, v in myDataDict.items():
                    print("{0}={1}".format(k, v))
            
            # write data to current logfile
            myLdl.writeLogfile(myDataDict) # pass in a dictionary with new fieldnames data every loop
            time.sleep(dt)
    
    
    except KeyboardInterrupt:
        logging.debug('caught KeyboardInterrupt, closing logfile and exiting!')
        e.set() # tell all threads to exit
        myLdl.e.set() # tell long duration logger threads to exit
        myLdl.fdLog.close() # close
        logging.debug("End   time: {0}".format(time.ctime()) )
        elapsedTime = time.time() - tStart # elapsed wall-clock time in seconds
        logging.debug("\n\nexiting after loopCnt={0}, eTime={1} seconds, nameCheckCnt={2}, nameChangeCnt={3}".format( loopCnt, elapsedTime, myLdl.nameCheckCnt, myLdl.nameChangeCnt ))








