### long_duration_logger

This is a long-duration python logging example to rotate the log file name at midnight everyday, or some other time interval. The intent is to use this log file name rotator for logging time series data over multiple days, months, and years.

This is not the same as a Linux system logfile rotator, but there are similarities. This is not part of an operating system logfile system, but rather, it is a Python 3 approach to long-duration data logging that spans multiple days, months, or years. In these cases, it is helpful to break up extended logging data files into separate logs, one for each day, with the date as the leading part of the filename.

Features:
  1. This updates a symbolic link that points to the current logfile.
  2. It also updates a sym link to the previous logfile (to simplify gathering the last 24 hours of data that spans two files)
  3. Logging program restarting is supported. This code will detect if the current logfile already exists and, if so, it will append new data and not add a header with fieldnames. If this is a new filename, it adds a header for easy .csv importing using Pandas [`read_csv()`](https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html).
  4. failures are detected and reported including inability to open or write to the logfile or differencs in dictionary fieldnames between specified at init vs. dictionary given each loop

The result is a series of logfiles from each day with the name beginning with YYYY_MM_DD_ as a prefix, where YYYY is the year, MM is the month (1-12) and DD is the day of the month, similar to this sequence with new files every minute:
![alt text](/screenshot_long_duration_logger_every_minute.png "Title")


## How it works

This code will work within an existing Python data logger loop, either as `main()` or in a thread.

A simple thread is launched at object initialization called `logfile_name_updater()`. This thread has a loop that pauses 10 seconds and compares the current date and time with the previous date and time. If there is a difference, bingo!, a queue message is sent to the methods used in the main logging loop.

If the current date and time string is different than the currently open file in the years, months, days, minutes, or seconds position, a new file name is chosen.

For logfile rotation:
  - every minute, set the comparison string to: `strftimeStr='%Y_%m_%d__%H_%M'`   <-- test with this to make sure your code is working
  - every hour  , set the comparison string to: `strftimeStr='%Y_%m_%d__%H'`
  - every day   , set the comparison string to: `strftimeStr='%Y_%m_%d'`   <-- use this for daily logfile rotation
  - every month , set the comparison string to: `strftimeStr='%Y_%m'`
  - every year  , set the comparison string to: `strftimeStr='%Y'`


tips:
  - test with `strftimeStr` set to change every minute, then change to every day when launching long-duration process
  - decide if you need `fdLog.flush()` or not; avoid `flush()` unless your cases has slow data logging and you want `tail -f` to update


## How to use it

The file `long_duration_logger_driver_tester.py` shows a running example. Download this file and the `daily_logfile_rotator.py` and run locally with:

`./long_duration_logger_driver_tester.py`


There are 2 steps:

1. Instrument your loop with the initialization:

    ```
    # ----- longDurationLogger setup -----
    strftimeStr='%Y_%m_%d'  # for daily rotation
    logDir='/var/tmp/wx'
    fieldnames=['tNow','Temp_C','Pressure_Pa','RH']
    myLdl = ldl(logDir,fieldnames,strftimeStr) # ldl-->long duration logger with filename update each day and symbolic link to current file
    ```

2. tick this method every loop you want to log data:

    ```
    # write data to current logfile
    myLdl.writeLogfile(myDataDict) # pass in a dictionary with new fieldnames data every loop
    ```




This code has been tested on Ubuntu 22.04 Linux with Python 3.10 but should work with any recent Python. It should work on filesystems that support symbolic links, like Linux, Mac, or Windows Subsystem for Linux (WSL).





***
### Software License: GNU Public License, version 3:
  - The long duration logger software is open-source and freely available under the terms of the GNU Public License, version 3 (GPLv3).
  - The GPLv3 is endorsed by the Open Source Initiative and is described here: https://opensource.org/licenses/gpl-3.0.html
  - The GPLv3 is a copyleft license which means you may redistribute it only under the same GPLv3 license terms.
    The intent behind the copyleft is to build a strong user community and ensure the original open-source software is
    not made proprietary or no longer open-source.
    Read more here about copyleft here: https://opensource.com/resources/what-is-copyleft



***
  Marc Compere, Ph.D.  
  comperem@erau.edu  
  created : 24 Sep 2023  
  modified: 04 Jan 2024


