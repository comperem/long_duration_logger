#!/usr/bin/env python3
#
# longDurationLogger driver testing - this should all be migrated to the __main__ portion of daily_logfile_rotator_v3.py
#
# mdc, 24 Sep 2023

import sys
import time
import random
import logging
import threading
from datetime import datetime
from daily_logfile_rotator import longDurationLogger as ldl

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] [%(threadName)-10s] %(message)s')

cnt=0
tStart=time.time()
debug=1 #0 # main()'s debug flag
dt=2 # (s)
skipNth=1000 # print every N'th cnt values
e = threading.Event()

# ----- longDurationLogger setup -----
# define comparison string; this determines log rotation every minute, hour, or day
strftimeStr='%Y_%m_%d__%H_%M' # str='2018_07_24__18_18'  <-- test with this b/c it changes every minute
#strftimeStr='%Y_%m_%d'        # str='2018_07_24'         <-- run for daily logfile rotation because it changes every day
logDir='/var/tmp/wx'
fieldnames=['tNow','Temp_C','Pressure_Pa','RH']
doFlush=True # flush file every write loop? yes for slow data log rate; no for fast data logging
myLdl = ldl(logDir,fieldnames,strftimeStr,doFlush) # ldl-->long duration logger with filename update each day and symbolic link to current file

try:
    logging.debug('Starting data collector loop')
    
    if debug>0: logging.debug("{0} listening...   cnt={1}, ipAddrStr={2}".format(sys.argv[0],cnt,myLdl.ipAddrStr))
    while e.is_set() is False:
        # print counter in a way that reduces terminal scrolling
        if (cnt % skipNth)==0:
            logging.debug('[{0}] long running process count {1}'.format(myLdl.ipAddrStr,cnt))
        else:
            print('{0},'.format(cnt),flush=True,end='')
        cnt+=1
        
        # collect data here; fictitious examples with random()
        T      = random.uniform(0.0,50.0)        # (deg C)
        P      = random.uniform(1013.05,1013.50) # (hPa) and (mBar) standard pressure at sea-level is 1013.25 in both millibars (mb) and hectopascal (hPa)
        RH     = random.uniform(+40,+100)        # (%)
        tNow   = '{0}'.format(datetime.now())    # '2023-09-07 09:36:36.741496'
        myDataDict={'tNow':datetime.now(), 'Temp_C':T, 'Pressure_Pa':P,'RH':RH}
        
        if debug>1:
            for k, v in myDataDict.items():
                print("{0}={1}".format(k, v))
        
        # write data to current logfile
        myLdl.writeLogfile(myDataDict) # pass in a dictionary with new fieldnames data every loop
        time.sleep(dt)


except KeyboardInterrupt:
    logging.debug('caught KeyboardInterrupt, closing logfile and exiting!')
    e.set() # tell all threads to exit
    myLdl.e.set() # tell long duration logger threads to exit
    myLdl.fdLog.close() # close
    logging.debug("End   time: {0}, cnt={1}".format(time.ctime(),cnt) )
    elapsedTime = time.time() - tStart # elapsed wall-clock time in seconds
    logging.debug("\n\nexiting after cnt={0}, eTime={0} seconds".format( cnt, elapsedTime ))

